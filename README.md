## Semana 27 - Geolocalización

1. Intro a geolocalización
2. Latitud y longitud
3. Detectando tu posición
4. Geocoding y Reverse Geocoding
5. Geocoding y Reverse con Rails
6. Proyecto base
7. Migración para latitud, longitud y dirección
8. Modificando el formulario de registro
9. Obteniendo la dirección en el click
10. Realizando el llamado Ajax
11. Agregar la dirección en el input
12. Mejorando la usabilidad con un spinner
13. Strong Params de Devise
14. Callback para Geocoding
15. API key de Google
16. Integrando Google Maps
17. Agregando tiendas
18. Mostrando las tiendas
19. Configurando Geocoder con el Key de Google
20. Centrando el mapa y zoom
21. Filtrando por cercania
22. AGregando eventos en el mapa
23. Reaccionando con AJAX
24. Fitrando los resultados

Para iniciar proyecto:
~~~
$ rails db:create
$ rails db:migrate
$ yarn install
~~~
